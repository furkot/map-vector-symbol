const mapVectorSymbol = require('../lib/fallback');

describe('map-vector-symbol fallback', function () {

  const PIN = 'M0 0H26V26H16L13 30L10 26H0Z';
  const STAR = 'M21.8 11.2L15.7 10.3 13 4.8 10.3 10.3 4.2 11.2 8.6 15.5 7.6 21.6 13 18.7 18.4 21.6 17.4 15.5 21.8 11.2Z';
  const SQAURE = 'M2 2H24V24H2Z';
  const SIZE =  { w: 26, h: 30 };

  it('toDataUrl', function () {
    let mvs = mapVectorSymbol(PIN, SQAURE, SIZE);
    let url = mvs.toDataURL('star', STAR, { color: 'green', reverse: false, });

    url.should.startWith('data:image/png;base64,');
  });

  it('getImageData', function (done) {
    let mvs = mapVectorSymbol(PIN, SQAURE, SIZE);
    mvs.getImageData('star', STAR, { color: 'green', reverse: false, }, check);

    function check(err, imageData) {
      imageData.should.have.property('width', 26);
      imageData.should.have.property('height', 30);
      imageData.should.have.property('data');
      done(err);
    }

  });

});

