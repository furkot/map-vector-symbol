const mapVectorSymbol = require('../lib/basic');

describe('map-vector-symbol basic', function () {

  const PIN = 'M0 0H26V26H16L13 30L10 26H0Z';
  const STAR = 'M21.8 11.2L15.7 10.3 13 4.8 10.3 10.3 4.2 11.2 8.6 15.5 7.6 21.6 13 18.7 18.4 21.6 17.4 15.5 21.8 11.2Z';
  const SQAURE = 'M2 2H24V24H2Z';
  const SIZE =  { w: 26, h: 30 };

  it('toDataUrl standard', function () {
    let mvs = mapVectorSymbol(PIN, SQAURE, SIZE);
    let url = mvs.toDataURL('star', STAR, { color: 'green', reverse: false });

    const prefix = 'data:image/svg+xml;utf8,';
    url.should.startWith(prefix);

    let svg = decodeURIComponent(url.slice(prefix.length));
    svg.should.be.eql('<svg xmlns="http://www.w3.org/2000/svg" width="26" height="30">' +
      '<path d="M0 0H26V26H16L13 30L10 26H0Z" fill="green" stroke="#333" stroke-width="0.5"/>' +
      '<path d="M21.8 11.2L15.7 10.3 13 4.8 10.3 10.3 4.2 11.2 8.6 15.5 7.6 21.6 13 18.7 18.4 21.6 17.4 15.5 21.8 11.2Z" fill="#fff"/>' +
    '</svg>');
  });

  it('toDataUrl reversed', function () {
    let mvs = mapVectorSymbol(PIN, SQAURE, SIZE);
    let url = mvs.toDataURL('star', STAR, { color: 'green', reverse: true });

    const prefix = 'data:image/svg+xml;utf8,';
    url.should.startWith(prefix);

    let svg = decodeURIComponent(url.slice(prefix.length));
    svg.should.be.eql('<svg xmlns="http://www.w3.org/2000/svg" width="26" height="30">' +
      '<path d="M0 0H26V26H16L13 30L10 26H0Z" fill="green"/>' +
      '<path d="M2 2H24V24H2Z" fill="#fff"/>' +
      '<path d="M21.8 11.2L15.7 10.3 13 4.8 10.3 10.3 4.2 11.2 8.6 15.5 7.6 21.6 13 18.7 18.4 21.6 17.4 15.5 21.8 11.2Z" fill="green"/>' +
    '</svg>');
  });

  it.skip('getImageData', function (done) {
    let mvs = mapVectorSymbol(PIN, SQAURE, SIZE);
    mvs.getImageData('star', STAR, { color: 'green', reverse: false, }, check);

    function check(err, imageData) {
      imageData.should.have.property('width', 26);
      imageData.should.have.property('height', 30);
      imageData.should.have.property('data');
      done(err);
    }
  });

});

