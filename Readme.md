[![NPM version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Dependency Status][deps-image]][deps-url]
[![Dev Dependency Status][deps-dev-image]][deps-dev-url]

# map-vector-symbol

Create map symbols from 2D paths.

## Install

```sh
$ npm install --save map-vector-symbol
```

## Usage

```js
var mapVectorSymbol = require('map-vector-symbol');

mapVectorSymbol('Rainbow');
```

## License

MIT © [Damian Krzeminski](https://furkot.com)

[npm-image]: https://img.shields.io/npm/v/map-vector-symbol.svg
[npm-url]: https://npmjs.org/package/map-vector-symbol

[travis-url]: https://travis-ci.org/furkot/map-vector-symbol
[travis-image]: https://img.shields.io/travis/furkot/map-vector-symbol.svg

[deps-image]: https://img.shields.io/david/furkot/map-vector-symbol.svg
[deps-url]: https://david-dm.org/furkot/map-vector-symbol

[deps-dev-image]: https://img.shields.io/david/dev/furkot/map-vector-symbol.svg
[deps-dev-url]: https://david-dm.org/furkot/map-vector-symbol?type=dev
