module.exports = makePath;

// implementation of new Path(str) constructor for IE

/* global Path2D */

function makePath(str) {
  let path = new Path2D();
  let x = 0, y = 0;

  str.replace(/(M|L|H|V|Z)([\s\.\d]*)/ig, function(m, command, args) {
    args = args.split(/\s+/).map(parseFloat);
    switch(command) {
      case 'M':
        for(let i = 0; i < args.length;) {
          x = args[i++];
          y = args[i++];
          path.moveTo(x, y);
        }
        break;
      case 'm':
        for(let i = 0; i < args.length;) {
          x += args[i++];
          y += args[i++];
          path.moveTo(x, y);
        }
        break;
      case 'L':
        for(let i = 0; i < args.length;) {
          x = args[i++];
          y = args[i++];
          path.lineTo(x, y);
        }
        break;
      case 'l':
        for(let i = 0; i < args.length;) {
          x += args[i++];
          y += args[i++];
          path.lineTo(x, y);
        }
        break;
      case 'H':
        x = args[0];
        path.lineTo(x, y);
        break;
      case 'h':
        x += args[0];
        path.lineTo(x, y);
        break;
      case 'V':
        y = args[0];
        path.lineTo(x, y);
        break;
      case 'v':
        y += args[0];
        path.lineTo(x, y);
        break;
      case 'Z':
      case 'z':
        path.closePath();
        break;
    }
  });

  return path;
}
