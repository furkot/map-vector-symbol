const parseCSSColor = require('csscolorparser').parseCSSColor;
const makePath = require('./path');

module.exports = mapVectorSymbol;

function mapVectorSymbol(shape, reverse, size) {

  function toDataURL(name, path, options) {
    return render(name, path, options).canvas.toDataURL("image/png");
  }

  function getImageData(name, path, options, fn) {
    fn(null, render(name, path, options).getImageData(0, 0, size.w, size.h));
  }

  const shapePath = makePath(shape);
  const reversePath = makePath(reverse);

  function fadeColor(color) {
    color = parseCSSColor(color);
    color[3] = 0.5;
    return `rgba(${color.join(',')})`;
  }

  function render(name, path, options) {
    const canvas = document.createElement('canvas');
    canvas.height = size.h;
    canvas.width = size.w;

    const ctx = canvas.getContext('2d');

    const color = options.fade ? fadeColor(options.color) : options.color;

    ctx.fillStyle = color;

    if (!options.reverse) {
      ctx.lineWidth = 0.5;
      ctx.strokeStyle = '#333';
      ctx.stroke(shapePath);
    }

    ctx.fill(shapePath);
    ctx.fillStyle = 'white';

    if (options.reverse) {
      ctx.fill(reversePath);
      ctx.fillStyle = color;
    }

    const contentPath = makePath(path);
    ctx.fill(contentPath);

    return ctx;
  }

  return {
    toDataURL,
    getImageData
  };
}
