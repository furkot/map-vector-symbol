const { xml } = require('el-component');

module.exports = mapVectorSymbol;

function mapVectorSymbol(shape, reverse, size) {

  function toDataURL(name, path, { reverse, fade, color }) {
    let svg = reverse ? iconReverse(color, path, fade) : iconNormal(color, path, fade);
    // return "data:image/svg+xml;base64," + btoa(svg);
    return "data:image/svg+xml;utf8," + encodeURIComponent(svg);
  }

  function getImageData(name, path,  options, fn) {
    const pixelRatio = window.devicePixelRatio || 1;

    let img = document.createElement('img');
    img.onload = onload;
    img.width = size.w * pixelRatio;
    img.height = size.h * pixelRatio;
    img.src = toDataURL(name, path, options);

    let canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;

    function onload() {
      let context = canvas.getContext('2d');
      context.drawImage(img, 0, 0, size.w, size.h);

      fn(null, context.getImageData(0, 0, size.w, size.h));
    }
  }

  function svgEnvelope(paths) {
    paths = paths.map(o => xml('path', o)).join('');

    return xml('svg', paths, {
      xmlns: "http://www.w3.org/2000/svg",
      width: size.w,
      height: size.h
    });
  }

  function iconNormal(fill, symbolPath, fade) {
    let pin = {
      d: shape,
      fill,
      stroke: '#333',
      'stroke-width': 0.5
    };
    let symbol = {
      d: symbolPath,
      fill: '#fff'
    };

    if (fade) {
      pin['fill-opacity'] = 0.5;
    }

    return svgEnvelope([pin, symbol]);
  }

  function iconReverse(fill, symbolPath, fade) {
    let pin = { d: shape, fill };
    let sq = { d: reverse, fill: '#fff' };
    let symbol = { d: symbolPath, fill };

    if (fade) {
      symbol['fill-opacity'] = 0.5;
    }

    return svgEnvelope([pin, sq, symbol]);
  }

  return {
    toDataURL,
    getImageData
  };
}
